package com.shariff.weather.entity;

public class CitySummary {
    private final Integer code;

    private final String icon;

    private final String temperature;

    public CitySummary(String city, Weather weather) {
        this.city = city;
        this.code = weather.getWeatherId();
        this.icon = weather.getWeatherIcon();
        this.temperature = String.format("%4.2f", weather.getTemperature());
    }

    private final String city;

    public String getCity() {
        return city;
    }

    public Integer getCode() {
        return code;
    }

    public String getIcon() {
        return icon;
    }

    public String getTemperature() {
        return temperature;
    }


}
