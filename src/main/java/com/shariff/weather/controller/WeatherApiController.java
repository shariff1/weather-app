package com.shariff.weather.controller;

import com.shariff.weather.entity.CitySummary;
import com.shariff.weather.entity.Weather;
import com.shariff.weather.service.WeatherService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/weather")
public class WeatherApiController {

    private final WeatherService weatherService;

    public WeatherApiController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @RequestMapping("/now/{country}/{city}")
    public Weather getWeather(@PathVariable String country,
                              @PathVariable String city) {
        return this.weatherService.getWeather(country, city);
    }

    @RequestMapping("/{city}")
    public ModelAndView getCityWeather(@RequestParam("city_name") String city) {
        Map<String, Object> model = new LinkedHashMap<String, Object>();
        model.put("citySummary", getSummary(city));
        return new ModelAndView("citySummary", model);
    }

    private Object getSummary(String city) {
        List<CitySummary> summary = new ArrayList<>();
        Weather weather = this.weatherService.getCityWeather(city);
        summary.add(new CitySummary(city, weather));
        return summary;
    }

}
