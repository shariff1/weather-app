package com.shariff.weather.service;

import com.shariff.weather.WeatherAppProperties;
import com.shariff.weather.entity.Weather;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import java.net.URI;

@Service
public class WeatherService {

    private static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?q={city},{country}&APPID={key}";

    private static final String CITY_URL = "http://api.openweathermap.org/data/2.5/weather?q={city}&APPID={key}";

    private static final Logger logger = LoggerFactory.getLogger(WeatherService.class);

    private final RestTemplate restTemplate;

    private final String apiKey;

    public WeatherService(RestTemplate restTemplate, WeatherAppProperties properties) {
        this.restTemplate = restTemplate;
        this.apiKey = properties.getApi().getKey();
    }

    @Cacheable("weather")
    public Weather getWeather(String country, String city) {
        URI url = new UriTemplate(WEATHER_URL).expand(city, country, this.apiKey);
        return invoke(url, Weather.class);
    }

    private <T> T invoke(URI url, Class<T> responseType) {
        RequestEntity<?> request = RequestEntity.get(url)
                .accept(MediaType.APPLICATION_JSON).build();
        ResponseEntity<T> exchange = this.restTemplate
                .exchange(request, responseType);
        return exchange.getBody();
    }

    @Cacheable("weather")
    public Weather getCityWeather(String city) {
        logger.info("Request for weather forecast for city", city);
        URI url = new UriTemplate(CITY_URL).expand(city, this.apiKey);
        return invoke(url, Weather.class);
    }
}
